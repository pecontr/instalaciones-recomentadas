# Instalaciones recomendadas - con Nest.js

<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

* [Visual Studio Code](https://code.visualstudio.com/)

* [Node](https://nodejs.org/en)

* [Docker Desktop](https://www.docker.com/get-started)

* [Git](https://git-scm.com/)
```
git config --global user.name "Tu nombre"
git config --global user.email "Tu correo"
```

* [Nest CLI](https://docs.nestjs.com/first-steps)
```
npm i -g @nestjs/cli
```


Opcional
* [Table Plus](https://tableplus.com/)

* [Mongo Compass](https://www.mongodb.com/try/download/shell)

* [Postman](https://www.postman.com/downloads/)

* [Insomnia](https://insomnia.rest/)


Descargar imagen de Mongo 5.0.0 y postgres 16
```
docker pull mongo:7.0
docker pull postgres:16.2
docker pull nats:latest
```

### Tema que estoy usando en VSCode:

* [Aura Theme](https://marketplace.visualstudio.com/items?itemName=DaltonMenezes.aura-theme)

* [Iconos](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)


Cambiar íconos de Angular por íconos de Nest -> Abrir: settings.json
```
"material-icon-theme.activeIconPack": "nest",
```

### Instalaciones adicionales

* [Paste JSON as Code](https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype)

* [.env](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv)

* [Better Dockerfile](https://marketplace.visualstudio.com/items?itemName=jeff-hykin.better-dockerfile-syntax)
